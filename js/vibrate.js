/**
 * @file
 * Javascript for the vibrate module.
 */

(function ($) {
  Drupal.behaviors.vibrate = {
    attach: function (context) {
      vibrate_values = Drupal.settings.vibrate.vibrate_values;
      navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
      navigator.vibrate([vibrate_values]);
      // Calculation for windows hight and width for popup.
      jQuery('.vibrate_popup_text').show();
      jQuery('.vibrate_popup_text').css('top', Math.max(0, ((jQuery(window).height() - jQuery('.vibrate_popup_text').outerHeight()) / 2) + jQuery(window).scrollTop()) + 'px');
      jQuery('.vibrate_popup_text').css('left', Math.max(0, ((jQuery(window).width() - jQuery('.vibrate_popup_text').outerWidth()) / 2) + jQuery(window).scrollLeft()) + 'px');
      jQuery('.vibrate_close').click(function ()
      {
        jQuery('.vibrate_popup_text').hide();
      });
    }
  }
})(jQuery);
