<?php
/**
 * @file
 * Provides the vibrate' administrative interface.
 */

/**
 * Creating form which save vibrate config data.
 *
 * @param array $form
 *   Array form will contain information about vibrate_form data.
 * @param array $form_submit
 *   Array get form values form submissions.
 */
function vibrate_form($form, &$form_submit) {
  $form['vibrate_front_page'] = array(
    '#title' => t('Vibrate on front page'),
    '#type' => 'checkbox',
    '#description' => t('Check if want vibration on front page'),
    '#option' => 1,
    '#default_value' => variable_get('vibrate_front_page', '0'),
    '#required' => FALSE,
  );
  $form['vibrate_node_values'] = array(
    '#title' => t('Vibrate node id'),
    '#type' => 'textfield',
    '#description' => t('Insert comma separated node id'),
    '#default_value' => variable_get('vibrate_node_values', '0'),
    '#required' => FALSE,
  );
  $form['vibrate_values'] = array(
    '#title' => t('Vibration value in microseconds ms'),
    '#type' => 'textfield',
    '#description' => t('Insert time value, how much time mobile should vibrate. Ex:- 50 for 50ms'),
    '#default_value' => variable_get('vibrate_values', '500'),
    '#required' => FALSE,
  );
  $form['vibrate_popup_text'] = array(
    '#title' => t('Vibrate popup text'),
    '#type' => 'textarea',
    '#description' => t('Insert text which should appear in popup with vibration, you can use HTML as well'),
    '#required' => TRUE,
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('vibrate_popup_text', t('Your vibrate text will come here.')),
  );
  return system_settings_form($form);
}
