vibrate Module
=======================================
Vibrate module provides you functionality to vibrate your mobile user's
mobile based on your selection with custom popup.
Selection could be home page or any node id.
Once user access your site a popup box with customized text
will appear with respect to selected node and mobile will vibrate.

Original idea is inherited from https://illyism.github.io/jquery.vibrate.js

The Vibration API is an API specifically made for mobile devices
as they are thankfully the only devices that have a vibrate function.
The API allows developers to vibrate a device(in a pattern)for a given duration.

Popup customization :- admin/config/services/vibrate

FAQ:-

1. What will happen for desktop user?
Desktop user will only see popup.
As vibrate js will work if device is having vibrate hardware.

2. Can I change popup look and feel.
Yes in module you will get a css file. you can change from there.

3. What if I need to load popup on home page?
There is an checkbox option in backend, please check on that and save it.

4. What if I don't want to load popup in desktop.
No that feature is not available till now.

However, this doesn't mean that your device can vibrate.
There are a few requirements you need to meet.

1. You need the hardware for it.
2. The page needs to be visible.
3. Browser-specific implementation prevents the vibration.
